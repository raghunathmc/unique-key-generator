let UniqueKeyGenerator;
if (typeof(UniqueKeyGenerator) == "undefined" || UniqueKeyGenerator == null || !UniqueKeyGenerator) {
    UniqueKeyGenerator = (function() {

        const getRandomInt = (max) => {
            return (Math.floor(Math.random() * Math.floor(max)));
        }
        let results = [];
        const alphabetArray = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];
        const getKey = (KeyVal) => {
            let valToReturn = '';
            if (KeyVal === 'a') {
                valToReturn = alphabetArray[getRandomInt(26)];
            } else {
                valToReturn = getRandomInt(10);
            }
            valToReturn = valToReturn+'';
            return valToReturn;
        }
        const defermaps = {
            '1': 'I',
            '8': 'B',
            '0': 'O',
            'O': '0',
            'B': '8',
            'I': '1'
        };
        const checkIfOk = (uniqueKey, resultString) => {
            let isOk = false;
            for (var i in defermaps) {
                if (uniqueKey === i) {
                    if (resultString.indexOf(defermaps[i]) > -1) {
                        isOk = true;
                    }
                }
            }
            return isOk;
        };
        const generateCombination = (iteration, element) => {
            let result = '';
            for (var i = 0; i < iteration; i++) {
                result = result + '' + element[getRandomInt(element.length)];
            }
            return result;
        }
        const binaryConvertedNumber = (number, iteration) => {
            const convertedBinary = parseInt(number).toString(2);
            let formattedConvertedBinary = convertedBinary + '';
            while (formattedConvertedBinary.length < iteration) {
                formattedConvertedBinary = '0' + formattedConvertedBinary;
            }
            return formattedConvertedBinary;
        }

        const mainResults = (iteration) => {
            const elementVal = ['a', 'd'];
            results = [];
            const combination = Math.pow(elementVal.length, iteration);
            for (var i = 0; i < combination; i++) {
                let combinationVal = binaryConvertedNumber(i, iteration);
                let converted = combinationVal.split('0').join('a');
                converted = converted.split('1').join('d');
                results.push(converted);
            }
            return results;
        }

        const mainFunction = (iteration) => {
            const combinationMap = mainResults(iteration);
            const combinationNumber = () => {
                return getRandomInt(Math.pow(2, iteration));
            }
            let selected_Combination = combinationMap[combinationNumber()];
            let resultString = '';
            selected_Combination.split('').forEach(element => {
                let uniqueKey = getKey(element);
                while (checkIfOk(uniqueKey, resultString)) {
                    uniqueKey = getKey(element);
                }

                resultString = resultString + uniqueKey;



            });
            return resultString;
        }
        const mainFunctionCombineKey = (keyLength, iterationNumber) => {
            let cobinationKey = '';
            for (var i = 0; i < iterationNumber; i++) {
                if (cobinationKey === '') {
                    cobinationKey = mainFunction(keyLength);
                } else {
                    cobinationKey = cobinationKey + "_" + mainFunction(keyLength);
                }

            }
            return cobinationKey;
        }

        return {
            generateUniqueKey: mainFunction,
            generateCombinedKey: mainFunctionCombineKey,


        }
    })();
}
