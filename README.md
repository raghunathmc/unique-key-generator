
# Unique-Key-Generator

  This JavaScript library is used to generate a unique alpha-numeric key with any provided length and to generate any combination key of different length with a provided number of occurrences.


# JavaScript files

 - uniqueFinder.uncompressed.js
		 This file is uncompressed version of JavaScript. 
		 
 - uniqueFinder.min.js
	  This file is the minified version of JavaScript

# How to Apply it in site
	Add below statement in html
    <script src="uniqueFinder.min.js"></script>
		
# How to Use it

 - To generate a unique key of a definite length use code as given below
 UniqueKeyGenerator.generateUniqueKey([Length of the Key]);
 For example : 
 

    UniqueKeyGenerator.generateUniqueKey(5);
    Result: V7UUO
    Try Again
    Result: 3DZ6Z
    Change the length to 10 ie.
    Result: 82261QLWW0
    
Please note that: 
	 -  Under 10 length result will come under  max of 100 ms ( for 5 digit its 2 ms)
	 - Under 16 length result will come under  max of 1000 ms
	 - beyond that it will Increase the responding time.
	 - If you are looking for bigger key , I prefer to use generateCombinedKey function

 - To generate a unique combination key of definite length and occurrence, use code as given below.
 

    UniqueKeyGenerator.generateCombinedKey([length of individual key],[occurrences count]);
For Example: 

     UniqueKeyGenerator.generateCombinedKey(5,4);
    Result: 5YI08_ZGXBR_O7329_52840
    Try Again
    Result: 3ZT9Q_0B9WV_54WQU_WM61J
    Change the length to  6 and count to 6 ie.
    Result: 0HYA57_V57G2L_4WU00M_LLT1GL_7O3S23_OV1JPC


# Restrictions applied

All the keys generated are having a rule applied were below pairs wont coexists in unique-key . Pairs are given below

 - 1 and I
 -  8 and B 
 - 0 and O 

To add more restriction modify the code and these pairs are kept in a map.
